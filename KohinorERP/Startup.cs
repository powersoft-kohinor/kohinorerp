﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KohinorERP.Startup))]
namespace KohinorERP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
